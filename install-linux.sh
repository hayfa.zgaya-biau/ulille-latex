#!/bin/bash

ROOT_DIR=$HOME
INSTALL_FONT_DIR=$ROOT_DIR/.fonts
INSTALL_TEX_DIR=$ROOT_DIR/texmf/tex/ULille

echo "copying tex files to $INSTALL_TEX_DIR"

mkdir -vp $INSTALL_TEX_DIR
cp -ru tex doc img CONTRIBUTING.md LICENSE LISEZMOI README $INSTALL_TEX_DIR

echo "copying font files to $INSTALL_FONT_DIR"

mkdir -vp $INSTALL_FONT_DIR
cp -ru fonts/* $INSTALL_FONT_DIR
fc-cache
