Fichiers de style pour LaTeX pour faire des documents respectant l'identité
visuelle de l'Université de Lille décrite à https://identite.univ-lille.fr/.

Trois types de documents sont proposés initialement : lettres, rapports et
présentations ; d'autres suivront.

Ces fichiers supposent une installation récente de LaTeX et ont été testés avec
les distributions texlive 2017 et 2021. Il faut utiliser lualatex pour compiler
les documents. Ce style nécessite l'installation des polices Marianne, Spectral
et Droid Sans Mono qui sont disponibles gratuitement pour tout système. Marianne
a été créée pour l'état français (https://www.gouvernement.fr/charte/charte-
graphique-les-fondamentaux/la-typographie) et peut être téléchargée à
https://gouvfr.atlassian.net/wiki/spaces/DB/pages/223019527/
Typographie+-+Typography. Droid Sans Mono a été créée pour Android
(http://en.wikipedia.org/wiki/Droid_fonts) et est disponible par exemple sur
http://www.fontsquirrel.com/fonts/droid-sans-mono ou le paquetage fonts-droid
des distributions Linux Ubuntu ou Debian. Spectral a été créée à l'initiative
de Google est est disponible par exemple sur https://www.fontsquirrel.com/
fonts/spectral. Pour vous simplifier la vie, ces polices de caractères sont
disponibles dans le répertoire fonts.

Contenu du répertoire :
  - tex/*.sty                Fichiers de style
  - doc/ex*                  Exemples
  - img/*                    Éléments graphiques (logos, fonds de pages)
  - fonts/*                  Polices de caractères
  - README                   LISEZMOI en anglais
  - LISEZMOI                 Ce fichier
  - LICENSE                  License copyleft GPL v3
  - install-linux.sh         script bash d'installation dans le répertoire
                             de l'utilisateur courant

Auteur initial : Pierre Boulet <Pierre.Boulet@univ-lille.fr>
